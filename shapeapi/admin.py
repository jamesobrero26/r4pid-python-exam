from django.contrib import admin
from shapeapi import models


class TriangleAdmin(admin.ModelAdmin):
    list_display = ['name', 'base', 'height', 'side_a', 'side_b']
    search_fields = ['name', 'base', 'height', 'side_a', 'side_b']


class RectangleAdmin(admin.ModelAdmin):
    list_display = ['name', 'length', 'width']
    search_fields = ['name', 'length', 'width']


class SquareAdmin(admin.ModelAdmin):
    list_display = ['name', 'side']
    search_fields = ['name', 'side']


class DiamonAdmin(admin.ModelAdmin):
    list_display = ['name', 'base', 'height', 'side']
    search_fields = ['name', 'base', 'height', 'side']


admin.site.register(models.TriangleModel, TriangleAdmin)
admin.site.register(models.RectangleModel, RectangleAdmin)
admin.site.register(models.SquareModel, SquareAdmin)
admin.site.register(models.DiamondModel, DiamonAdmin)
