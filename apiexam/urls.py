"""apiexam URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.contrib import admin
from django.urls import path, include
from shapeapi import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("user", views.UserViewSet, basename='user')
router.register("triangle", views.TriangleViewSet, basename='triangle')
router.register("rectangle", views.RectangleViewSet, basename='rectangle')
router.register("square", views.SquareViewSet, basename='square')
router.register("diamond", views.DiamondViewSet, basename='diamond')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
]
